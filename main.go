package main

import (
	"fmt"
	"net"
	"net/url"
	"strings"
)

/*
net - The Go Programming Language https://golang.org/pkg/net/
dialとlistenがある。

Go言語でTCPのEchoサーバーを実直に実装する - Qiita 2017年09月19日に更新 https://qiita.com/kawasin73/items/3371d35166af733c2ce4
handle
どこにgo routineを使うか
*/

func handleConnection(conn net.Conn) {
	defer conn.Close()

	/*
		たしか \r\n\r\n こないと終了しないので、これが発見され次第終了する？
	*/
	println("called handleConnection!")
	buf := make([]byte, 4*1024) // これ、4KBくらい？
	stockString := ""
	// なぜかfor
	for {
		if strings.Contains(stockString, "\r\n\r\n") {
			println("found \\r\\nX2!!!")
			if strings.Contains(stockString, "GET /") {
				httpOkString := "HTTP/1.1 200 OK\r\n"
				httpOkString += "Content-Type: text/html\r\n"
				httpOkString += "\r\n"
				htmlString := `
<html>
<style>
body {
width: 800px;
margin: auto;
}
body h1 {
	color: red;
}
body p {
	background-color: pink;
}
</style>
<body>
<h1>
	Title: helloworld! 
</h1>
<div>
  <p>hey this is p tag.</p>
</div>
</body>
</html>
`
				httpOkString += htmlString + "\r\n"
				httpOkString += "\r\n"
				conn.Write([]byte(httpOkString))
				println("send httpOkString")
				// conn.Close()
				return
			} else {
				conn.Write([]byte("failed...\r\n"))
			}
			stockString = ""
			continue
		}

		n, err := conn.Read(buf)
		if err != nil {
			println("もしかしたら、ここで接続終了かも？")
			println("read error!")
			fmt.Printf("n はzero ? %d\n", n)
			fmt.Printf("もしかしたらbufは? : %s\n", string(buf[:n]))
			return
			// panic(err)
		}
		stockString += string(buf[:n])

		receivedString := string(buf[:n])
		fmt.Printf("received msg: %s\n", receivedString)
		q := url.QueryEscape(receivedString)
		fmt.Printf("encoded: %s\n", q)

		fmt.Printf("is equal to \\r\\n: %t\n", receivedString == "GET / HTTP/1.1\r\n")

		// エコーしてみる
		// n, err = conn.Write(buf[:n])
		// if err != nil {
		// 	println("echo failed!")
		// 	panic(err)
		// }
		// println("echo succeeded! : " + strconv.Itoa(n))
	}
}
func main() {
	port := ":8080"
	ln, err := net.Listen("tcp", port)
	if err != nil {
		println("cannot listen port: " + port)
		panic(err)
	}
	println("hello! listened port : " + port)

	for {
		conn, err := ln.Accept()
		if err != nil {
			println("failed to accept...")
			panic(err)
		}
		go handleConnection(conn)
	}
}
